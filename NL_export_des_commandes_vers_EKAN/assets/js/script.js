(function($){

    $(document).ready(() => {

        const startDate = document.getElementById('startDateExportedCommand');
        const endDate = document.getElementById('endDateExportedCommand');
        const dateSelector = document.getElementById('datesSelector');
        const submitButton = document.querySelector('.submit');
        const warningMessage = document.getElementById('warning_message');

        startDate.addEventListener('change', function(){
            console.log(submitButton);
            compareDateStartAndEnd(startDate.value, endDate.value);
        });

        endDate.addEventListener('change', function(){
            console.log(submitButton);
            compareDateStartAndEnd(startDate.value, endDate.value);
        });

        function compareDateStartAndEnd(startString, endString){
            const startDate = new Date(startString);
            const endDate = new Date(endString);
            if (startDate >= endDate){

                // warningMessage.classList.remove('hide');
                warningMessage.style.display = "block";
                
                //verrouiller le bouton submit
                
            } else {

                // warningMessage.classList.add('hide');
                warningMessage.style.display = "none";

                //dé verrouiller le bouton submit
                
            }
        }
    });

})(jQuery);