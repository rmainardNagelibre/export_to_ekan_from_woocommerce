<?php

function makeFillAndTransfertXML(array $p_aArrayOrder){
    if ( !empty($p_aArrayOrder) ) {

        /**
         * initiate the XML file
         */
        $xmlDoc = new DOMDocument('1.0', 'utf-8');
        $xmlCommandes = $xmlDoc->createElement('Commandes');
        $xmlDoc->appendChild($xmlCommandes);
    
        /**
         * add each order form the array obtain by the query in the XML file
         */
        foreach ($p_aArrayOrder as $order) {
            $xmlCommande = $xmlDoc->createElement('Commande');
            $xmlCommande->setAttribute('Deleted',''); 
            $xmlCommandes->appendChild($xmlCommande);
    
            $IDEcommerçant = $xmlDoc->createElement('IDEcommercant', '08014');
            $IDTransporteur = $xmlDoc->createElement('IDTransporteur', convertShippingMethodeToCode($order->get_shipping_method()) );
            $NrCommande = $xmlDoc->createElement('NrCommande', $order->get_data()['number']);
    
            $DtCommande = $xmlDoc->createElement('DtCommande', $order->get_date_paid()->format('Y-m-d'));
            $RefClient = $xmlDoc->createElement('RefClient', $order->get_user_id());
            $NomFacturation = $xmlDoc->createElement('NomFacturation', $order->get_billing_last_name());
            $PrenomFacturation = $xmlDoc->createElement('PrenomFacturation', $order->get_billing_first_name());
            $AdresseFacturation = $xmlDoc->createElement('AdresseFacturation', $order->get_billing_address_1());
            $Adresse2Facturation = $xmlDoc->createElement('Adresse2Facturation', filterContentXMLLine($order->get_billing_address_2()));
            $CodePostalFacturation = $xmlDoc->createElement('CodePostalFacturation',$order->get_billing_postcode());
            $VilleFacturation = $xmlDoc->createElement('VilleFacturation', $order->get_billing_city());
            $PaysFacturation = $xmlDoc->createElement('PaysFacturation', $order->get_billing_country());
            $TelephoneFacturation = $xmlDoc->createElement('TelephoneFacturation', $order->get_billing_phone());
            $EmailFacturation  = $xmlDoc->createElement('EmailFacturation', $order->get_billing_email());
    
            $NomLivraison = $xmlDoc->createElement('NomLivraison', $order->get_shipping_last_name());
            $PrenomLivraison = $xmlDoc->createElement('PrenomLivraison', $order->get_shipping_first_name());
            $AdresseLivraison = $xmlDoc->createElement('AdresseLivraison', $order->get_shipping_address_1());
            $AdresseLivraison2 = $xmlDoc->createElement('Adresse2Livraison', filterContentXMLLine($order->get_shipping_address_2()));
            $CodePostalLivraison = $xmlDoc->createElement('CodePostalLivraison', $order->get_shipping_postcode());
            $VilleLivraison  = $xmlDoc->createElement('VilleLivraison', $order->get_shipping_city());
            $PaysLivraison  = $xmlDoc->createElement('PaysLivraison', $order->get_shipping_country());
            $MobileLivraison  = $xmlDoc->createElement('MobileLivraison', $order->get_billing_phone()); 
            $EmailLivraison   = $xmlDoc->createElement('EmailLivraison', $order->get_billing_email()); 
            $NrDepot  = $xmlDoc->createElement('NrDepot', filterContentXMLLine(getIdRepository(convertShippingMethodeToCode($order->get_shipping_method()))));
            $Commentaire  = $xmlDoc->createElement('Commentaire', filterContentXMLLine($order->get_data()['customer_note']));
            $Contenu  = $xmlDoc->createElement('Contenu' );
            
            //billing section
            $xmlCommande->appendChild($IDEcommerçant);
            $xmlCommande->appendChild($IDTransporteur);
            $xmlCommande->appendChild($NrCommande);
            $xmlCommande->appendChild($DtCommande);
            $xmlCommande->appendChild($RefClient);
            $xmlCommande->appendChild($NomFacturation);
            $xmlCommande->appendChild($PrenomFacturation);
            $xmlCommande->appendChild($AdresseFacturation);
            $xmlCommande->appendChild($Adresse2Facturation);
            $xmlCommande->appendChild($CodePostalFacturation);
            $xmlCommande->appendChild($VilleFacturation);
            $xmlCommande->appendChild($PaysFacturation);
            $xmlCommande->appendChild($TelephoneFacturation);
            $xmlCommande->appendChild($EmailFacturation);
            
            // Delivery section
            $xmlCommande->appendChild($NomLivraison);
            $xmlCommande->appendChild($PrenomLivraison);
            $xmlCommande->appendChild($AdresseLivraison);
            $xmlCommande->appendChild($AdresseLivraison2);
            $xmlCommande->appendChild($CodePostalLivraison);
            $xmlCommande->appendChild($VilleLivraison);
            $xmlCommande->appendChild($PaysLivraison);
            $xmlCommande->appendChild($MobileLivraison);
            $xmlCommande->appendChild($EmailLivraison);
            $xmlCommande->appendChild($NrDepot);
            $xmlCommande->appendChild($Commentaire);
            
            //article section
            $xmlCommande->appendChild($Contenu);
    
            /**
             * foreach product in the order
             */
            foreach($order->get_data()['line_items'] as $product){
                
                $sUGS = $product->get_product()->get_data()['sku'];
                $sProductQuantity = $product->get_data()['quantity'];
                
                //create the XML elements
                $Ligne = $xmlDoc->createElement('Ligne');
                $RefArticle = $xmlDoc->createElement('RefArticle', $sUGS);
                $QuantiteCommande = $xmlDoc->createElement('QuantiteCommande',$sProductQuantity);
                
                //Add the XML elements in the XML file
                $Contenu->appendChild($Ligne);
        
                $Ligne->appendChild($RefArticle);
                $Ligne->appendChild($QuantiteCommande);
        
            }
        }
    
        $sXmlFile = '08014-'.date('Y').date('m').date('d').'-'.date('H').date('i').date('s').'.xml';
        $xmlDoc->save(dirname(__DIR__, 1 ) . '/'. $sXmlFile);
    
        transfertFileWithFTP($sXmlFile);
    }
    else {
        echo '<h3>pIl n\'y a pas de commandes à exporter !</h3>';
    }
}

/**
 * function to display the opening and closing tag on the XML file
 * @param string $p_sContent
 * @return string 
 */
function filterContentXMLLine(string $p_sContent):string {
    if ($p_sContent == ''){
        return ' ';
    } 
    return $p_sContent;
}

/**
 * convert the shipping string to a number in a string
 *
 * @param string $p_sShipping
 * @return string 
 */
function convertShippingMethodeToCode(string $p_sShipping): string
{
    if (A_SHIPPING_CODE[$p_sShipping] !== null) {
        return A_SHIPPING_CODE[$p_sShipping];
    } else {
        $data = 'Erreur de code transport sur la commande : ' . $order->get_id();
        return file_put_contents( 'errorsDuringExport.xml', $data ,FILE_APPEND);
    }
    ;
}

/**
 * if the shipping code is 'Point relay', it search the
 *  NrDepot of the 'point relay'
 * @param string $p_ishippingId
 * @return string 
 */
function getIdRepository(string $p_ishippingId): string
{
    if ($p_ishippingId == '23'){
        return 'afficher ID Point Mondial Relay !';
        //donc aller chercher le code dans les données
    }
    else {
        return '';
    }
}

/**
 * funtion for transfert the generated XML file to the shipper
 * @param string $p_sFile
 * @return bool 
 */
function transfertFileWithFTP(string $p_sFile): bool
{

    //open the ftp connection
    $ftp_server = 'ftp.ekan-centre.fr';
    $ftp_user_name = 'nagelibre';
    $ftp_user_pass = 'NGb2p51sZk';
    $conn_id = ftp_connect($ftp_server) or die("Couldn't connect to $ftp_server");

    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

    if ((!$conn_id) || (!$login_result)) {
        var_dump( "La connexion FTP a échoué !");
        var_dump( "Tentative de connexion au serveur $ftp_server pour l'utilisateur $ftp_user_name");
        exit;
    } else {
        echo( "<p>Connexion au serveur $ftp_server, pour l'utilisateur $ftp_user_name</p>");
    }

    ftp_pasv($conn_id, true);

    $destination_file = 'commande/' . $p_sFile;
    $source_file = dirname(__DIR__, 1 ) . '/' . $p_sFile;
    // transfert the file via FTP
    $upload = ftp_put($conn_id, $destination_file, $source_file, FTP_BINARY); 

    // verify the downloading status
    if (!$upload) {
        var_dump( "Le chargement FTP a échoué!");
        return false;
    } else {
        echo( "<p style=\"color:#009900; font-weight:bold;\">Le chargement de $source_file vers $ftp_server en tant que $destination_file a bien été effectué</p>");
        // unlink($source_file);
        return true;
    }

    //close the ftp connection
    ftp_close($conn_id);
}