<?php
/*
Plugin Name: NL_export_des_commandes_vers_EKAN
Description: Ceci est un plugin réalisé pour exporter les commandes vers Ekan via une connexion FTP
Author: Aymeric SCHOEBEL
Version:1.2
*/

include 'assets/make_XML.php' ;

const A_SHIPPING_CODE = [
    'Livraison gratuite par code promo' => '52',
    'Livraison gratuite' => '61',
    'Livraison gratuite pour masque' => '52',
    'Livraison gratuite en club' => '20',
    'Livraison personnelle/Retirer en magasin' => '20',
    'Livraison personnelle/Remis en main propre' => '20',
    'Point Relay' => '23',
    'GLS flex' => '61',
    'Colissimo Corse' => '1'
]; 

/**
 * add entry for the plugin for the left menu in the admin back office
 */
function NL_exportOrderEkanCreateMenu(){
    add_menu_page( 'Export commandes vers Ekan', 'Export commandes vers Ekan', 'administrator', __FILE__, 'NL_exportOrderEkanGenerateSettingsPage',  'dashicons-migrate');
}

/**
 * add hook for left menu in the admin back office
 */
add_action( 'admin_menu', 'NL_exportOrderEkanCreateMenu');

/**
 * code html of the admin page of the plugin
 */
function NL_exportOrderEkanGenerateSettingsPage()
{ 
    ?>
    <div class="NL_export_order_ekan_wrap">
        <div class="NL_export_order_ekan_inner">
            <h2>Export des commandes vers Ekan </h2>           
            <form action="" method="post">
                <fieldset style ="border: solid;">
                <legend> <h3>Réglages</h3> </legend> 

                <!-- settings_fields to keep the settings in an array whitch will be send to database when the form is submit -->
                <?php //settings_fields('NL_export_order_ekan-settings-group'); ?>

                    <div id="datesSelector">
                        <h4>Plage de dates</h4>
                        <input type="date" id="startDateExportedCommand" name="startDateExportedCommand"
                            value="<?php echo date('Y-m-d', strtotime("-1 day")) ?>"
                            min="2018-01-01" max="<?php echo date('Y-m-d', strtotime("-1 day")) ?>">
                        <?php //$dStartDateExportedCommand = esc_attr(get_option('startDateExportedCommand')); ?>
                        vers 
                        <input type="date" id="endDateExportedCommand" name="endDateExportedCommand"
                            value="<?php echo date('Y-m-d') ?>"
                            min="2018-01-02" max="<?php echo date('Y-m-d') ?>">
                        <?php //$dEndDateExportedCommand = esc_attr(get_option('endDateExportedCommand')); ?>
                        <p id="warning_message" style="display: none;">La date de début ne doit pas être égale ou supérieure à la date de fin !</p>
                    </div>

                    <div>
                        <input type="checkbox" name="mark_orders_as_exported" id="mark_orders_as_exported" value="true" checked>
                        <label for="mark_orders_as_exported">Marquer les commandes comme exportées</label>
                        <?php //$bMarkOrderExportedCheckboxValue = esc_attr(get_option('mark_orders_as_exported')); ?>
                    </div>
                    <div>
                        <input type="checkbox" name="export_unmarked_orders_only" id="export_unmarked_orders_only" value="true" checked>
                        <label for="export_unmarked_orders_only">Exporter les commandes non marquées uniquement</label>
                        <?php //$bExportUnmarkedOrdersCheckboxValue = esc_attr(get_option('export_unmarked_orders_only')); ?>
                    </div>

                    <?php submit_button('Exporter les commandes'); ?>   
                </fieldset>
            </form>
        </div>
    </div>

    <?php
    
    if (isset($_POST)){
        //settings store in an array
        $aSettings = array(
            'dStartDateExportedCommand' => $_POST['startDateExportedCommand'],
            'dEndDateExportedCommand' => $_POST['endDateExportedCommand'],
            'bMarkOrderExportedCheckboxValue' => $_POST['mark_orders_as_exported'],
            'bExportUnmarkedOrdersCheckboxValue' => $_POST['export_unmarked_orders_only'],
        );
    
        /**
         * to make the XML file when submit form and not at the initialisation of the admin page
         */
        if ($_POST) {
            $aData = getOrders($aSettings);
            makeFillAndTransfertXML($aData);
        }    
    }
    
}


/**
 * function for add some settings on the WP database
 */
function NL_exportOrderEkanRegisterSettings(){
    register_setting( 'NL_export_order_ekan-settings-group', 'startDateExportedCommand');
    register_setting( 'NL_export_order_ekan-settings-group', 'endDateExportedCommand');
    register_setting( 'NL_export_order_ekan-settings-group', 'mark_orders_as_exported');
    register_setting( 'NL_export_order_ekan-settings-group', 'export_unmarked_orders_only');
}
add_action('admin_post', 'NL_exportOrderEkanRegisterSettings');

//fonction pour ajouter les scripts et le style pour le back admin
function NL_exportOrderEkanAddStyleAndScripts() {
    if (get_admin_page_title() === 'Export commandes vers Ekan') {
        wp_enqueue_style( 'NL_export_order_ekan_style', plugins_url('assets/css/style.css', __FILE__) );
        wp_enqueue_script( 'NL_export_order_ekan_script', plugins_url('assets/js/script.js', __FILE__), array('jquery', 'wp-color-picker') );
    } 
}
add_action( 'admin_enqueue_scripts', 'NL_exportOrderEkanAddStyleAndScripts' );


/***************************************************** */

/**
 * get all the orders of the database
 * @param array $p_aSettings
 * @return array
 */
function getOrders(array $p_aSettings) : array
{

    $aDataForXML = [];

    $dDateStart = $p_aSettings['dStartDateExportedCommand'];
    $dEndStart = $p_aSettings['dEndDateExportedCommand'];

    /**
     * set the arguments for the query to get the orders
     */
    $to_date = strtotime('+1 day', strtotime($dEndStart . '00:00:00'));
    $aArgs = array(
        'date_created' => strtotime( $dDateStart. '00:00:00') .'...'.$to_date,
        'status' => array('wc-processing', 'wc-completed'),
        'limit' => '10000'
    );

    /**
     * get the orders
     */
    $aArrayOrdersFromQuery = wc_get_orders($aArgs);

    /**
     * get the details of the order with the WooCommerce functions
     */
    $iLength = count($aArrayOrdersFromQuery);
    for ($i = 0 ; $i < $iLength ; $i++){
        $iIdOrder = $aArrayOrdersFromQuery[$i]->get_id();

        /**
         * if settings 'Exporter les commandes non marquées uniquement' is true 
         */
        if ($p_aSettings['bExportUnmarkedOrdersCheckboxValue']) {
            
            // check if the order is NOT exported yet to export it
            if (empty(get_post_meta($iIdOrder, 'woe_order_exported'))) {
                $aDataForXML = putOrderInArrayForExport( $aDataForXML, $iIdOrder);       
                
            } 
        } else {
            /**
             * Export the order without checking if it's mark as exported or not
             */
            $order = wc_get_order( $iIdOrder );
            $aDataForXML = putOrderInArrayForExport( $aDataForXML, $iIdOrder);            
        }
        // if settings 'Marquer les commandes comme exportées' is true
        if ($p_aSettings['bMarkOrderExportedCheckboxValue']){
            //mark the order as exported   
            $order = wc_get_order( $iIdOrder );     
            tryMarkOrder($iIdOrder, $p_aSettings);
        }

    }

    return $aDataForXML;

}

/**
 * function from plugin/woocommerce/classes/core/class-wc-order-export-engine.php 
 * if the setting is true, mark the order whith a timestamp when exported
 *
 * @param int $iOrderId
 * @param array $p_aSettings
 * @return bool
 */
function tryMarkOrder( $iOrderId, $p_aSettings ) : bool {
	if ( $p_aSettings['bMarkOrderExportedCheckboxValue'] ) {
		update_post_meta( $iOrderId, 'woe_order_exported', current_time( 'timestamp' ) );
        return true;
	}
    return false;
}

/**
 * push the order in the array to export in XML
 *
 * @param array $p_aArray
 * @param integer $p_iIdOrder
 * @return array
 */
function putOrderInArrayForExport(array $p_aArray, int $p_iIdOrder): array{
    $order = wc_get_order( $p_iIdOrder );
    $p_aArray[] = $order;

    return $p_aArray;
}